package com.myfriends

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.myfriends.domain.models.Person
import com.myfriends.presentation.adapters.FriendsAdapter
import com.myfriends.presentation.states.FriendsState
import com.myfriends.presentation.viewmodels.FriendsViewModel
import kotlinx.android.synthetic.main.activity_frieds.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class FriendsActivity : AppCompatActivity() {

    private val friendsViewModel: FriendsViewModel by viewModel()
    private lateinit var friendsAdapter: FriendsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frieds)
        setupRecyclerView()
        observerEvents()
        friendsViewModel.getFriends()
    }

    private fun observerEvents() {
        friendsViewModel.getViewState().observe(this, Observer { friendsState ->
            when (friendsState) {
                is FriendsState.Success -> {
                    friendsAdapter.items = friendsState.friends
                }
                is FriendsState.ErrorReturnMessage -> {
                    Toast.makeText(this, friendsState.message, Toast.LENGTH_LONG).show()
                }
                is FriendsState.ErrorReturnResource -> {
                    Toast.makeText(this, getString(friendsState.resource), Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun setupRecyclerView() {
        friendsAdapter = FriendsAdapter(onItemSelected = this::onPersonSelected)
        rcvFriends.apply {
            layoutManager = LinearLayoutManager(context)
            isMotionEventSplittingEnabled = false
            setHasFixedSize(true)
            adapter = friendsAdapter
        }
    }

    private fun onPersonSelected(person: Person) {

    }
}
