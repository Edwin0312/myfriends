package com.myfriends.app

import android.app.Application
import com.myfriends.presentation.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class AppController : Application() {
    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidLogger()
            androidContext(this@AppController)
            modules(
                listOf(
                    apiModule,
                    viewModelModule,
                    repositoryModule,
                    useCaseModule,
                    databaseModule
                )
            )
        }
    }
}