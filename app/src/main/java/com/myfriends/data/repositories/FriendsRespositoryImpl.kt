package com.myfriends.data.repositories

import com.myfriends.data.db.dao.PersonDao
import com.myfriends.data.db.entities.PersonEntity
import com.myfriends.data.endpoints.FriendsApi
import com.myfriends.domain.models.Person
import com.myfriends.domain.repositories.FriendsRepository

class FriendsRespositoryImpl(
    private val friendsApi: FriendsApi,
    private val personDao: PersonDao
) : FriendsRepository {
    /**
     * This method make a friends request in remote end point
     */
    override fun getFriends() = friendsApi.getFriends()

    /**
     * This method makes a friends request in the local database
     */
    override fun getFriendsLocal() = personDao.getFriends()

    override fun saveFriends(friends: List<Person>) {
        val friendsLocal = mutableListOf<PersonEntity>()
        friends.forEach {
            val personLocal = PersonEntity(
                gender = it.gender
            )
            friendsLocal.add(personLocal)
        }
        personDao.saveFriends(friendsLocal)
    }


}