package com.myfriends.data.models

import com.squareup.moshi.Json


data class PersonResponse(
    @Json(name = "info")
    var info: Info? = Info(),
    @Json(name = "results")
    var results: List<Result?>? = listOf()
)

data class Info(
    @Json(name = "page")
    var page: Int? = 0,
    @Json(name = "results")
    var results: Int? = 0,
    @Json(name = "seed")
    var seed: String? = "",
    @Json(name = "version")
    var version: String? = ""
)

data class Result(
    @Json(name = "cell")
    var cell: String? = "",
    @Json(name = "dob")
    var dob: Dob? = Dob(),
    @Json(name = "email")
    var email: String? = "",
    @Json(name = "gender")
    var gender: String? = "",
    @Json(name = "id")
    var id: Id? = Id(),
    @Json(name = "location")
    var location: Location? = Location(),
    @Json(name = "login")
    var login: Login? = Login(),
    @Json(name = "name")
    var name: Name? = Name(),
    @Json(name = "nat")
    var nat: String? = "",
    @Json(name = "phone")
    var phone: String? = "",
    @Json(name = "picture")
    var picture: Picture? = Picture(),
    @Json(name = "registered")
    var registered: Registered? = Registered()
)

data class Dob(
    @Json(name = "age")
    var age: Int? = 0,
    @Json(name = "date")
    var date: String? = ""
)

data class Id(
    @Json(name = "name")
    var name: String? = "",
    @Json(name = "value")
    var value: String? = ""
)

data class Location(
    @Json(name = "city")
    var city: String? = "",
    @Json(name = "coordinates")
    var coordinates: Coordinates? = Coordinates(),
    @Json(name = "country")
    var country: String? = "",
    @Json(name = "state")
    var state: String? = "",
    @Json(name = "street")
    var street: Street? = Street(),
    @Json(name = "timezone")
    var timezone: Timezone? = Timezone()
)

data class Coordinates(
    @Json(name = "latitude")
    var latitude: String? = "",
    @Json(name = "longitude")
    var longitude: String? = ""
)

data class Street(
    @Json(name = "name")
    var name: String? = "",
    @Json(name = "number")
    var number: Int? = 0
)

data class Timezone(
    @Json(name = "description")
    var description: String? = "",
    @Json(name = "offset")
    var offset: String? = ""
)

data class Login(
    @Json(name = "md5")
    var md5: String? = "",
    @Json(name = "password")
    var password: String? = "",
    @Json(name = "salt")
    var salt: String? = "",
    @Json(name = "sha1")
    var sha1: String? = "",
    @Json(name = "sha256")
    var sha256: String? = "",
    @Json(name = "username")
    var username: String? = "",
    @Json(name = "uuid")
    var uuid: String? = ""
)

data class Name(
    @Json(name = "first")
    var first: String? = "",
    @Json(name = "last")
    var last: String? = "",
    @Json(name = "title")
    var title: String? = ""
)

data class Picture(
    @Json(name = "large")
    var large: String? = "",
    @Json(name = "medium")
    var medium: String? = "",
    @Json(name = "thumbnail")
    var thumbnail: String? = ""
)

data class Registered(
    @Json(name = "age")
    var age: Int? = 0,
    @Json(name = "date")
    var date: String? = ""
)