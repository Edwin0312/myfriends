package com.myfriends.data.endpoints

import com.myfriends.data.models.PersonResponse
import io.reactivex.Single
import retrofit2.http.GET

interface FriendsApi {
    /**
     * Make a request to the web service to get a list of friends.
     */
    @GET("/api/?results=50")
    fun getFriends(): Single<PersonResponse>
}