package com.myfriends.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.myfriends.data.db.entities.PersonEntity

@Dao
interface PersonDao {
    @Query("SELECT * FROM person")
    fun getFriends(): List<PersonEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveFriends(people: List<PersonEntity>)
}