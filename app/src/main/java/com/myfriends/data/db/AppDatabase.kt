package com.myfriends.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.myfriends.data.db.dao.PersonDao
import com.myfriends.data.db.entities.PersonEntity

@Database(
    version = 1, entities = [
        PersonEntity::class
    ]
)

abstract class AppDatabase : RoomDatabase() {
    abstract fun personDao(): PersonDao
}