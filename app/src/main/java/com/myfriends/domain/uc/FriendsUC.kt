package com.myfriends.domain.uc

import com.myfriends.domain.models.Person
import com.myfriends.domain.repositories.FriendsRepository
import io.reactivex.Single

class FriendsUC(
    private val friendsRepository: FriendsRepository
) {
    /**
     * This method retunr a observable of the request remote, for get a list of friends
     */
    fun getFriends(): Single<List<Person>> = friendsRepository.getFriends()
        .flatMap {
            val people: MutableList<Person> = mutableListOf()
            it.results?.forEach { noNullResult ->
                noNullResult?.run {
                    people.add(
                        Person(
                            gender = gender
                        )
                    )
                }
            }
            friendsRepository.saveFriends(people)
            Single.just(people)
        }
}