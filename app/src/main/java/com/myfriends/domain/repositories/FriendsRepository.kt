package com.myfriends.domain.repositories

import com.myfriends.data.db.entities.PersonEntity
import com.myfriends.data.models.PersonResponse
import com.myfriends.domain.models.Person
import io.reactivex.Single

interface FriendsRepository {

    /**
     * Retrieve a list of friends
     * @return Single<ResponseUser>: A [Single] which is going to issue the currently empty data store
     */
    fun getFriends(): Single<PersonResponse>

    fun getFriendsLocal(): List<PersonEntity>

    fun saveFriends(friends: List<Person>)
}