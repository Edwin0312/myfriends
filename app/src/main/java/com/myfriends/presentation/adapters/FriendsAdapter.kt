package com.myfriends.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.myfriends.R
import com.myfriends.domain.models.Person
import kotlinx.android.synthetic.main.item_recycler_person.view.*
import kotlin.properties.Delegates

class FriendsAdapter(
    items: List<Person> = emptyList(), private val onItemSelected: ((Person) -> Unit)? = null
) : RecyclerView.Adapter<FriendsAdapter.ViewHolder>() {

    var items: List<Person> by Delegates.observable(
        initialValue = items,
        onChange = { _, _, _ -> notifyDataSetChanged() }
    )


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_recycler_person,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items[position].also { person ->
            run {
                holder.view.txvPersonName.text = person.gender
                holder.view.setOnClickListener {
                    onItemSelected?.invoke(person)
                }
            }
        }
    }
}