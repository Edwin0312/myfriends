package com.myfriends.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import com.myfriends.R
import com.myfriends.domain.uc.FriendsUC
import com.myfriends.presentation.base.BaseViewModel
import com.myfriends.presentation.extensions.getTypeServiceError
import com.myfriends.presentation.states.FriendsState
import com.myfriends.presentation.states.ServiceException
import io.reactivex.Scheduler

class FriendsViewModel(
    private val friendsUC: FriendsUC,
    private val viewState: MutableLiveData<FriendsState>,
    private val back: Scheduler,
    private val front: Scheduler
) : BaseViewModel() {

    fun getViewState() = viewState

    fun getFriends() {
        addDisposable(friendsUC.getFriends()
            .doOnSubscribe { viewState.postValue(FriendsState.Loading) }
            .subscribeOn(back)
            .observeOn(front)
            .subscribe({ people ->
                viewState.value = FriendsState.Success(people)
            }, {
                when (val serviceException = it.getTypeServiceError()) {
                    is ServiceException.ClientException<*> -> {
                        viewState.value = FriendsState.ErrorReturnResource(R.string.error_request)
                    }
                    is ServiceException.ServerException -> {
                        viewState.value =
                            FriendsState.ErrorReturnResource(serviceException.resource)
                    }
                    is ServiceException.OtherException -> {
                        viewState.value = FriendsState.ErrorReturnResource(R.string.error_request)
                    }
                }
            })
        )
    }
}