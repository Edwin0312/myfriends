package com.myfriends.presentation.states

sealed class ServiceException {
    class ClientException<T>(val data: T) : ServiceException()
    class ServerException(val resource: Int) : ServiceException()
    class OtherException(val throwable: Throwable) : ServiceException()
}