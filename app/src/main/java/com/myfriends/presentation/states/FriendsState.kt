package com.myfriends.presentation.states

import com.myfriends.domain.models.Person

sealed class FriendsState {
    object Loading : FriendsState()
    data class Success(val friends: List<Person>) : FriendsState()
    data class ErrorReturnMessage(val message: String) : FriendsState()
    class ErrorReturnResource(val resource: Int) : FriendsState()
}