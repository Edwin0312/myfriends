package com.myfriends.presentation.extensions

import com.myfriends.R
import com.myfriends.presentation.states.ServiceException
import retrofit2.HttpException
import java.net.HttpURLConnection

fun Throwable.getTypeServiceError(): ServiceException {
    return if (this is HttpException) {
        when (this.code()) {
            in HttpURLConnection.HTTP_BAD_REQUEST until HttpURLConnection.HTTP_INTERNAL_ERROR -> {
                ServiceException.ClientException(this)
            }
            HttpURLConnection.HTTP_INTERNAL_ERROR -> {
                ServiceException.ServerException(R.string.error_http_default)
            }
            else -> {
                ServiceException.OtherException(this)
            }
        }
    } else {
        ServiceException.OtherException(this)
    }
}
