package com.myfriends.presentation.di

import androidx.room.Room
import com.myfriends.data.db.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single {
        Room.databaseBuilder(androidContext(), AppDatabase::class.java, "friends_database").build()
    }
    single { get<AppDatabase>().personDao() }
}