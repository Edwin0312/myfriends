package com.myfriends.presentation.di

import com.myfriends.data.endpoints.FriendsApi
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

val apiModule: Module = module {
    single {
        RetrofitClientFactory.getAuthenticationRetrofitInstance(androidContext())
            .create(FriendsApi::class.java)
    }
}