package com.myfriends.presentation.di

import android.content.Context
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.myfriends.R
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClientFactory {

    private const val DEFAULT_TIMEOUT_SECONDS: Long = 60

    /**
     * Returns a Retrofit instance with an interceptor for Logging HTTP requests and responses.
     */
    private fun getDefaultInstance(baseUrl: String): Retrofit {
        //Interceptor
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder().addInterceptor(interceptor)
            .connectTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .build()

        val moshi = Moshi.Builder().build()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    /**
     * Return a Retrofit instance for cretate the authentication endpoint
     */
    fun getAuthenticationRetrofitInstance(context: Context) =
        getDefaultInstance(context.getString(R.string.base_url_randomuser))

}