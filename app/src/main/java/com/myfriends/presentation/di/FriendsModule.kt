package com.myfriends.presentation.di

import androidx.lifecycle.MutableLiveData
import com.myfriends.data.repositories.FriendsRespositoryImpl
import com.myfriends.domain.repositories.FriendsRepository
import com.myfriends.domain.uc.FriendsUC
import com.myfriends.presentation.viewmodels.FriendsViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val viewModelModule: Module = module {
    viewModel {
        FriendsViewModel(
            friendsUC = get(),
            viewState = MutableLiveData(),
            back = Schedulers.io(),
            front = AndroidSchedulers.mainThread()
        )
    }
}
val useCaseModule: Module = module {
    factory { FriendsUC(friendsRepository = get()) }
}

val repositoryModule: Module = module {
    factory<FriendsRepository> { FriendsRespositoryImpl(friendsApi = get(), personDao = get()) }
}